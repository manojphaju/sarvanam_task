﻿using Sarvanam_Task.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sarvanam_Task.DataAccess
{
    public class db
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);

        
        public DataSet ShowAllRecord()
        {
            SqlCommand com = new SqlCommand("ProcGetEmployees", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void Add_Record(Employee employee)
        {
            SqlCommand com = new SqlCommand("ProcInsertUpdateEmployee", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@FirstName", employee.FirstName);
            com.Parameters.AddWithValue("@LastName", employee.LastName);
            com.Parameters.AddWithValue("@Gender", employee.Gender);
            com.Parameters.AddWithValue("@Salary", employee.Salary);
            com.Parameters.AddWithValue("@UserId", employee.UserId);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();

        }


        public DataSet GetRecordById(int id)
        {
            SqlCommand com = new SqlCommand("ProcGetEmployeeById", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

       

        public void DeleteRecordById(int id)
        {
            SqlCommand com = new SqlCommand("ProcDeleteEmployee", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
        }

    }
}