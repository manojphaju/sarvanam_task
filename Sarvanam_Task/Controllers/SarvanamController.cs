﻿using Sarvanam_Task.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Mvc.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace Sarvanam_Task.Controllers
{
    public class SarvanamController : ApiController
    {
        DataAccess.db con = new DataAccess.db();

        [Route("api/Sarvanam/GetAllRecords")]
        [HttpGet]
        public DataSet GetAllRecords()
        {
            DataSet ds = con.ShowAllRecord();
            return ds;
        }

        [Route("api/Sarvanam/GetEmployeeRecordById")]
        [HttpGet]
        public DataSet GetEmployeeRecordById(int id)
        {
            DataSet ds = con.GetRecordById(id);
            return ds;
        }

        [Route("api/Sarvanam/AddEmployeeRecord")]
        [HttpPost]
        public HttpResponseMessage AddEmployeeRecord([FromBody]Employee fc)
        {
            Employee emp = new Employee();
            emp.FirstName = fc.FirstName;
            emp.LastName = fc.LastName;
            emp.Salary = fc.Salary;
            emp.Gender = fc.Gender;
            emp.UserId = fc.UserId;
            con.Add_Record(emp);
            return Request.CreateResponse(HttpStatusCode.Created, emp);
        }

        

        [Route("api/Sarvanam/DeleteEmployeeRecord")]
        [HttpPut]
        public HttpResponseMessage DeleteEmployeeRecord(int id)
        {
            con.DeleteRecordById(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Record Deleted");
        }
    }
}
