USE [master]
GO
/****** Object:  Database [Sarvanam]    Script Date: 12/7/2019 11:23:59 AM ******/
CREATE DATABASE [Sarvanam]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Sarvanam', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\Sarvanam.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Sarvanam_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\Sarvanam_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Sarvanam] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Sarvanam].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Sarvanam] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Sarvanam] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Sarvanam] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Sarvanam] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Sarvanam] SET ARITHABORT OFF 
GO
ALTER DATABASE [Sarvanam] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Sarvanam] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Sarvanam] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Sarvanam] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Sarvanam] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Sarvanam] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Sarvanam] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Sarvanam] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Sarvanam] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Sarvanam] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Sarvanam] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Sarvanam] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Sarvanam] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Sarvanam] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Sarvanam] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Sarvanam] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Sarvanam] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Sarvanam] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Sarvanam] SET  MULTI_USER 
GO
ALTER DATABASE [Sarvanam] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Sarvanam] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Sarvanam] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Sarvanam] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Sarvanam] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Sarvanam]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Gender] [varchar](10) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeLog]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Action] [nchar](10) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeRequestLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeSalary]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSalary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Salary] [nchar](10) NOT NULL,
 CONSTRAINT [PK_EmployeeSalary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[ProcDeleteEmployee]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[ProcDeleteEmployee]
@Id int,
@UserId int = 1
As
Begin
	Delete from EmployeeSalary where EmployeeId = @Id;
	Delete from Employee where Id = @Id
	Insert into EmployeeLog
		([EmployeeId] ,[Action]  ,[DateTime]   ,[UserId])
		Values
			(@Id,'Delete',GETDATE(),@UserId)	
	if cast(@@ROWCOUNT as int) > 0
	 Select 1
	 Else
	  select 0
End

GO
/****** Object:  StoredProcedure [dbo].[ProcGetEmployeeById]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ProcGetEmployeeById]
@Id int,
@UserId int =1
As
Begin

 Insert into EmployeeLog
	([EmployeeId] ,[Action]  ,[DateTime]   ,[UserId])
	select Id,'Read',GETDATE(),@UserId from Employee where Id = @Id
	
 SELECT Employee.Id
      ,[FirstName]
      ,[LastName]
      ,[Gender]
	  ,IsNull([Salary] ,0) Salary
  FROM [Employee]
  Left Join EmployeeSalary 
  on Employee.Id = EmployeeSalary.EmployeeId where Employee.Id = @Id
End

GO
/****** Object:  StoredProcedure [dbo].[ProcGetEmployees]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ProcGetEmployees]
@UserId int =1
As
Begin

 Insert into EmployeeLog
	([EmployeeId] ,[Action]  ,[DateTime]   ,[UserId])
	select Id,'Read',GETDATE(),@UserId from Employee
	
 SELECT Employee.Id
      ,[FirstName]
      ,[LastName]
      ,[Gender]
	  ,IsNull([Salary] ,0) Salary
  FROM [Employee]
  Left Join EmployeeSalary 
  on Employee.Id = EmployeeSalary.EmployeeId
End

GO
/****** Object:  StoredProcedure [dbo].[ProcInsertUpdateEmployee]    Script Date: 12/7/2019 11:23:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ProcInsertUpdateEmployee]
@Id int = null,
@FirstName nvarchar(50),
@LastName nvarchar(50),
@Gender nvarchar(10),
@Salary decimal,
@UserId int = 1
As
Begin

Declare @action nvarchar(10) = 'Insert';
	if( select top 1 Id from Employee where Id = @Id) is null
	Begin
		Insert into Employee 
			(FirstName, LastName,Gender)
		Values
			(@FirstName,@LastName,@Gender);
		set @Id = (select SCOPE_IDENTITY());

		Insert into EmployeeSalary
			(EmployeeId,Salary)
		Values
			(@Id,@Salary);

	End
	Else
	Begin
		set @action = 'Update'
		Update Employee set
			FirstName = @FirstName,
			LastName = @LastName,
			Gender = @Gender
		Where Id = @Id
		
		Update EmployeeSalary set
			Salary = @Salary
		 where EmployeeId = @Id
		
	End

	Insert into EmployeeLog
		([EmployeeId] ,[Action]  ,[DateTime]   ,[UserId])
		Values
			(@Id,@action,GETDATE(),@UserId)
	
	Select @Id
End

GO
USE [master]
GO
ALTER DATABASE [Sarvanam] SET  READ_WRITE 
GO
